package com.afomic.spark;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.AsyncTaskLoader;
import androidx.loader.content.Loader;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afomic.spark.activities.MainActivity;
import com.afomic.spark.adapter.BlogDisplayAdapter;
import com.afomic.spark.adapter.CommentAdapter;
import com.afomic.spark.data.Constants;
import com.afomic.spark.data.PreferenceManager;
import com.afomic.spark.model.BlogElement;
import com.afomic.spark.model.BlogPost;
import com.afomic.spark.model.Comment;
import com.afomic.spark.util.ElementParser;
import com.afomic.spark.util.FirebaseTracker;
import com.afomic.spark.util.HidingScrollLinearListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BlogDetailActivity extends AppCompatActivity
        implements LoaderManager.LoaderCallbacks<ArrayList<BlogElement>> {
    private static final String PARAM_BLOG_HTML = "html";
    private static final int BLOG_ELEMENT_LOADER_ID = 101;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.rv_blog_detail)
    RecyclerView blogView;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    @BindView(R.id.fab_like)
    FloatingActionButton likeFab;
    @BindView(R.id.show_comments_card)
    CardView showCommentCard;
    @BindView(R.id.show_comment_progress)
    ProgressBar showCommentsProgress;
    @BindView(R.id.tv_show_comment)
    TextView showCommentsTextView;
    @BindView(R.id.comment_layout)
    CardView addCommentLayout;
    @BindView(R.id.edt_comment)
    EditText commentEditText;
    @BindView(R.id.rv_comments)
    RecyclerView commentListView;
    LinearLayoutManager mLayoutManager;
    BlogPost mBlogPost;
    ArrayList<BlogElement> mBlogElements;
    ArrayList<Comment> mComments = new ArrayList<>();
    BlogDisplayAdapter mAdapter;
    CommentAdapter mCommentAdapter;
    private PreferenceManager mPreferenceManager;
    private Query commentDatabaseRef;
    private ChildEventListener commentEventListener;
    private DatabaseReference likeRef;
    private boolean postIsLiked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blog_detail);
        ButterKnife.bind(this);
        mBlogPost = getIntent().getParcelableExtra(Constants.EXTRA_BLOG_POST);
        mPreferenceManager = new PreferenceManager();
        initView();
        initListener();
        if (mBlogPost == null) {
            mProgressBar.setVisibility(View.VISIBLE);
            FirebaseDynamicLinks.getInstance()
                    .getDynamicLink(getIntent())
                    .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                        @Override
                        public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                            if (pendingDynamicLinkData != null) {
                                Uri deepLink = pendingDynamicLinkData.getLink();
                                String post_id = deepLink.getQueryParameter(Constants.EXTRA_BLOG_ID);
                                FirebaseDatabase.getInstance().getReference(Constants.POST_REF)
                                        .child(mPreferenceManager.getAssociationName())
                                        .child(post_id)
                                        .addValueEventListener(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                mBlogPost = dataSnapshot.getValue(BlogPost.class);
                                                if (mBlogPost.getType() == BlogPost.Type.BLOG) {
                                                    loadBlog();
                                                }

                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) {

                                            }
                                        });
                            }

                        }
                    })
                    .addOnFailureListener(this, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.w("log", "getDynamicLink:onFailure", e);
                        }
                    });
        } else {
            loadBlog();
        }
        FirebaseTracker.trackScreen(BlogDetailActivity.this, "BlogDetailActivity");

    }

    public void initView() {
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("Nacoss");
        }
        mBlogElements = new ArrayList<>();
        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new BlogDisplayAdapter(BlogDetailActivity.this, mBlogElements, null);
        blogView.setLayoutManager(mLayoutManager);
        blogView.setAdapter(mAdapter);

    }

    public void initListener() {
        blogView.addOnScrollListener(new HidingScrollLinearListener(mLayoutManager) {
            @Override
            public void onHide() {
                CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams) likeFab.getLayoutParams();
                int fabBottomMargin = lp.bottomMargin;
                likeFab.animate().translationY(likeFab.getHeight() + fabBottomMargin).setInterpolator(new AccelerateInterpolator(2)).start();
            }

            @Override
            public void onShow() {
                likeFab.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();

            }

            @Override
            public void onLoadMore(int current_page) {

            }
        });
    }

    public void loadBlog() {
        LoaderManager loadManager = getSupportLoaderManager();
        Bundle args = new Bundle();
        args.putString(PARAM_BLOG_HTML, mBlogPost.getBody());
        final Loader<ArrayList<BlogElement>> mBlogLoader = loadManager.getLoader(BLOG_ELEMENT_LOADER_ID);
        if (mBlogLoader == null) {
            loadManager.initLoader(BLOG_ELEMENT_LOADER_ID, args, this);
        } else {
            loadManager.restartLoader(BLOG_ELEMENT_LOADER_ID, args, this);
        }
        likeRef = FirebaseDatabase.getInstance()
                .getReference(Constants.LIKES_REF)
                .child(mBlogPost.getId());
        likeRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild(mPreferenceManager.getUserId())) {
                    postIsLiked = true;
                    likeFab.setImageResource(R.drawable.ic_favorite);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        setupComment();
        increaseBlogViews();
    }

    public void setupComment() {
        mCommentAdapter = new CommentAdapter(BlogDetailActivity.this, mComments);
        LinearLayoutManager commentLayoutManger = new LinearLayoutManager(BlogDetailActivity.this);
        commentListView.setLayoutManager(commentLayoutManger);
        commentListView.setAdapter(mCommentAdapter);
        commentDatabaseRef = FirebaseDatabase.getInstance()
                .getReference(Constants.COMMENT_REF)
                .orderByChild("blogId")
                .equalTo(mBlogPost.getId());
        commentEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, String s) {
                Comment comment = dataSnapshot.getValue(Comment.class);
                mComments.add(comment);
                mCommentAdapter.notifyItemInserted(mComments.size() - 1);

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
    }

    @NonNull
    @Override
    public Loader<ArrayList<BlogElement>> onCreateLoader(int id, Bundle args) {
        return new parseBlogElement(this, args);
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<BlogElement>> loader, ArrayList<BlogElement> data) {
        mProgressBar.setVisibility(View.GONE);
        mBlogElements.clear();
        mBlogElements.addAll(data);
        mBlogElements.remove(mBlogElements.size() - 1);
        mBlogElements.remove(mBlogElements.size() - 1);
        mAdapter.notifyDataSetChanged();
        addCommentLayout.setVisibility(View.VISIBLE);
        showCommentCard.setVisibility(View.VISIBLE);
    }

    public void increaseBlogViews() {
        FirebaseDatabase.getInstance()
                .getReference(Constants.POST_VIEW_REF)
                .child(mBlogPost.getId())
                .child(mPreferenceManager.getUserId())
                .setValue(true);
    }

    public void loadComment() {
        commentDatabaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChildren()) {
                    showCommentCard.setVisibility(View.GONE);
                    commentListView.setVisibility(View.VISIBLE);
                } else {
                    showCommentsProgress.setVisibility(View.GONE);
                    showCommentsTextView.setVisibility(View.VISIBLE);
                    showCommentsTextView.setText(R.string.no_comment_title);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        commentDatabaseRef.addChildEventListener(commentEventListener);
    }

    @Override
    public void onLoaderReset(@NonNull Loader<ArrayList<BlogElement>> loader) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (commentDatabaseRef != null) {
            commentDatabaseRef.removeEventListener(commentEventListener);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.fab_like)
    public void likePost() {
        if (postIsLiked) {
            postIsLiked = false;
            likeFab.setImageResource(R.drawable.ic_favorite_border);
            likeRef.child(mPreferenceManager.getUserId())
                    .removeValue();
        } else {
            postIsLiked = true;
            likeFab.setImageResource(R.drawable.ic_favorite);
            likeRef.child(mPreferenceManager.getUserId()).setValue(true);
        }
    }

    @OnClick(R.id.show_comments_card)
    public void showComments() {
        showCommentsProgress.setVisibility(View.VISIBLE);
        showCommentsTextView.setVisibility(View.GONE);
        loadComment();
    }

    @OnClick(R.id.btn_submit_comment)
    public void submitComment() {
        String content = commentEditText.getText().toString();
        if (!content.isEmpty()) {
            final ProgressDialog dialog = new ProgressDialog(BlogDetailActivity.this);
            dialog.setMessage("Posting Comment......");
            Comment comment = new Comment();
            DatabaseReference commentRef = FirebaseDatabase.getInstance()
                    .getReference(Constants.COMMENT_REF);

            String commentId = commentRef.push().getKey();
            comment.setBlogId(mBlogPost.getId());
            comment.setId(commentId);
            comment.setContent(content);
            comment.setPosterName(mPreferenceManager.getUsername());
            comment.setTimeStamp(System.currentTimeMillis());
            commentRef.child(commentId)
                    .setValue(comment).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    dialog.dismiss();
                    Toast.makeText(BlogDetailActivity.this, "Comment Posted", Toast.LENGTH_SHORT).show();
                }
            });
            commentEditText.setText("");
        } else {
            Toast.makeText(BlogDetailActivity.this, "Empty Comment", Toast.LENGTH_SHORT).show();

        }


    }

    @Override
    public void onBackPressed() {
        Intent intent=new Intent(BlogDetailActivity.this,MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY);
        startActivity(intent);
    }

    public static class parseBlogElement extends AsyncTaskLoader<ArrayList<BlogElement>> {
        private Bundle param;

        public parseBlogElement(Context context, Bundle param) {
            super(context);
            this.param = param;
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

        @Override
        public ArrayList<BlogElement> loadInBackground() {
            return ElementParser.fromHtml(param.getString(PARAM_BLOG_HTML));
        }
    }
}