package com.afomic.spark.fragment;

import android.Manifest;
import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.afomic.spark.BlogDetailActivity;
import com.afomic.spark.R;
import com.afomic.spark.adapter.PostAdapter;
import com.afomic.spark.data.Constants;
import com.afomic.spark.data.PreferenceManager;
import com.afomic.spark.model.BlogPost;
import com.afomic.spark.model.NativeAd;
import com.afomic.spark.util.FirebaseDynamicLinkHelper;
import com.afomic.spark.util.FirebaseTracker;
import com.afomic.spark.util.HidingScrollLinearListener;
import com.afomic.spark.util.PostHelper;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.afomic.spark.data.Constants.POST_REF;

/**
 * Created by afomic on 11/14/17.
 */

public class PostFragment extends Fragment implements PostAdapter.BlogPostListener {
    private static Map<Long, Integer> downloadRef = new HashMap<>();
    @BindView(R.id.rv_post_list)
    RecyclerView postRecyclerView;
    @BindView(R.id.empty_layout)
    LinearLayout emptyViewLayout;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    @BindView(R.id.swipe_container)
    SwipeRefreshLayout swipeContainer;
    ArrayList<Object> mPostList;
    PostAdapter mAdapter;
    Unbinder mUnbinder;
    private PreferenceManager mPreferenceManager;
    private DownloadBroadcastReceiver mDownloadBroadcastReceiver;
    private PostHelper postHelper;
    private LinearLayoutManager layoutManager;

    public static final String TAG = "postFragment";

    public static PostFragment newInstance() {
        return new PostFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPostList = new ArrayList<>();
        mPreferenceManager = new PreferenceManager();
        requestPermission();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_post, container, false);
        mUnbinder = ButterKnife.bind(this, v);
        postHelper = new PostHelper(mPreferenceManager.getAssociationName());
        FirebaseTracker.trackScreen(getActivity(), "PostFragment");
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        initDownloadReceiver();
        loadPosts();
        initListener();
    }

    public void initView(View v) {
        AppCompatActivity act = (AppCompatActivity) getActivity();
        ActionBar actionBar = act.getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("Home");
        }

        mAdapter = new PostAdapter(getActivity(), mPostList, this);
        layoutManager = new LinearLayoutManager(getContext());
        postRecyclerView.setLayoutManager(layoutManager);
        postRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(),DividerItemDecoration.VERTICAL));
        postRecyclerView.setAdapter(mAdapter);

        // check if the list is empty
        DatabaseReference mDatabaseReference = FirebaseDatabase.getInstance()
                .getReference(POST_REF)
                .child(mPreferenceManager.getAssociationName());
        mDatabaseReference
                .orderByChild("status")
                .equalTo(Constants.STATUS_APPROVED)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (mProgressBar != null) {
                            mProgressBar.setVisibility(View.GONE);
                        }

                        if (!dataSnapshot.hasChildren()) {
                            if (emptyViewLayout != null) {
                                postRecyclerView.setVisibility(View.GONE);
                                emptyViewLayout.setVisibility(View.VISIBLE);
                            }
                        } else {
                            if (emptyViewLayout != null) {
                                emptyViewLayout.setVisibility(View.GONE);
                            }

                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    public void initDownloadReceiver() {
        IntentFilter filter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        mDownloadBroadcastReceiver = new DownloadBroadcastReceiver();
        getActivity().registerReceiver(mDownloadBroadcastReceiver, filter);
    }

    public void initListener() {
        postRecyclerView.addOnScrollListener(new HidingScrollLinearListener(layoutManager) {
            @Override
            public void onHide() {

            }

            @Override
            public void onShow() {
                postRecyclerView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadMore(int current_page) {
                postHelper.getBlogPost(current_page, new PostHelper.BlogPostCallBack() {
                    @Override
                    public void onSuccess(List<Object> blogPosts) {
                        int startPosition = mPostList.size();
                        mPostList.addAll(blogPosts);
                        mAdapter.notifyItemRangeInserted(startPosition, blogPosts.size());
                    }

                    @Override
                    public void onFailure(String message) {

                    }
                });
            }
        });

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPostList.clear();
                mAdapter.notifyDataSetChanged();
                loadPosts();
            }
        });
    }

    public void loadPosts() {
        postHelper.getBlogPost(1, new PostHelper.BlogPostCallBack() {
            @Override
            public void onSuccess(List<Object> blogPosts) {
                mPostList.addAll(blogPosts);
                mAdapter.notifyDataSetChanged();
                swipeContainer.setRefreshing(false);
            }

            @Override
            public void onFailure(String message) {

            }
        });
    }

    @Override
    public void onDestroyView() {
        mUnbinder.unbind();
        getActivity().unregisterReceiver(mDownloadBroadcastReceiver);
        super.onDestroyView();

    }

    @Override
    public void OnFileBlogPostClick(int position) {
        BlogPost blogPost = (BlogPost) mPostList.get(position);
        if (downloadRef.containsValue(blogPost.getId())) {// file is already been downloaded
            Toast.makeText(getActivity(), "File is Already been downloaded", Toast.LENGTH_SHORT).show();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.READ_EXTERNAL_STORAGE) !=
                        PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{
                                    android.Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
                            }, 100);
                } else {
                    downloadFile(blogPost, position);
                }
            } else {
                downloadFile(blogPost, position);
            }

        }


    }

    public void downloadFile(BlogPost blogPost, int position) {
        blogPost.setDownloading(true);
        mAdapter.notifyItemChanged(position);
        File direct = new File(Environment.getExternalStorageDirectory()
                + "/Nacoss/doc");
        if (!direct.exists()) {
            direct.mkdirs();
        }
        Uri file_uri = Uri.parse(blogPost.getFileUrl());
        DownloadManager downloadManager = (DownloadManager) getContext().getSystemService(Context.DOWNLOAD_SERVICE);
        DownloadManager.Request request = new DownloadManager.Request(file_uri);
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
        request.setAllowedOverRoaming(false);
        request.setTitle("Downloading " + blogPost.getTitle());
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setVisibleInDownloadsUi(true);
        request.setDestinationInExternalPublicDir("Nacoss/doc", blogPost.getTitle());

        long refid = downloadManager.enqueue(request);
        downloadRef.put(refid, position);
        updateStats(blogPost.getId());
    }

    @Override
    public void onBlogBlogPostClick(BlogPost BlogPost) {
        Intent intent = new Intent(getActivity(), BlogDetailActivity.class);
        intent.putExtra(Constants.EXTRA_BLOG_POST, BlogPost);
        startActivity(intent);

    }

    @Override
    public void onSharePost(BlogPost post) {
        mProgressBar.setVisibility(View.VISIBLE);
        FirebaseDynamicLinkHelper.getDynamicLink(post, new FirebaseDynamicLinkHelper.DynamicLinkCallback() {
            @Override
            public void onFinish(String shortUrl) {
                mProgressBar.setVisibility(View.GONE);
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, shortUrl);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                try {
                    startActivity(Intent.createChooser(intent, "Share Post"));
                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                mProgressBar.setVisibility(View.GONE);
                Log.e("tag", "onError: " + error);
            }
        });
    }

    public void requestPermission() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.READ_EXTERNAL_STORAGE) !=
                    PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{
                                android.Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
                        }, 100);
            }
        }


    }

    public void updateStats(String postId) {
        FirebaseDatabase.getInstance()
                .getReference(Constants.LIKES_REF)
                .child(postId)
                .child(mPreferenceManager.getUserId())
                .setValue(true);

    }

    public class DownloadBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            long referenceId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);

            int position = downloadRef.get(referenceId) == null ? 0 : downloadRef.get(referenceId);
            Object object = mPostList.get(position);
            if (object instanceof BlogPost) {
                BlogPost blogPost = (BlogPost) object;
                if (blogPost.getType() == BlogPost.Type.FILE) {
                    blogPost.setDownloading(false);
                    mAdapter.notifyItemChanged(position);
                    Toast.makeText(getActivity(), "Download Complete", Toast.LENGTH_SHORT).show();
                }
            }

        }
    }


    @Override
    public void onNativeAdActionClick(NativeAd nativeAd) {
        String url = nativeAd.getCallToActionUrl();
        if (url != null) {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        }


    }
}
