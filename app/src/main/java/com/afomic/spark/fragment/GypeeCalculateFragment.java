package com.afomic.spark.fragment;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.view.MenuItemCompat;
import androidx.appcompat.widget.SearchView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Toast;

import com.afomic.spark.R;
import com.afomic.spark.activities.AddCourseActivity;
import com.afomic.spark.adapter.GridAdapter;
import com.afomic.spark.adapter.SuggestionAdapter;
import com.afomic.spark.data.Constants;
import com.afomic.spark.data.CourseHelper;
import com.afomic.spark.model.Course;
import com.afomic.spark.model.Gypee;
import com.afomic.spark.util.FirebaseTracker;
import com.afomic.spark.util.NonScrollListView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


/**
 * Created by afomic on 22-Oct-16.
 */
public class GypeeCalculateFragment extends Fragment {
    private List<Course> userCourse;
    private List<Course> selectedCourse;
    private Gypee userGypee;
    private int semester, level;
    private double totalPoint, totalUnit;
    private int tempTotalUnit;
    private GridAdapter adapter;
    private SuggestionAdapter searchAdapter;
    private NonScrollListView gradingList;
    private GridLayout layout;
    private CourseHelper mCourseHelper;
    private Callback mCallback;
    private SearchView courseSearch;
    private ListView searchList;
    private LinearLayout searchLayout;
    private Button addNewCourse;
    private ScrollView scrollContainer;
    private MenuItem menuItem;

    public static GypeeCalculateFragment getInstance(int level, int semester, double totalPoint, double totalUnit) {
        GypeeCalculateFragment fragment = new GypeeCalculateFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.LEVEL, level);
        bundle.putInt(Constants.SEMESTER, semester);
        bundle.putDouble(Constants.TOTAL_POINT, totalPoint);
        bundle.putDouble(Constants.TOTAL_UNIT, totalUnit);
        fragment.setArguments(bundle);
        return fragment;

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallback = (Callback) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arg = getArguments();
        level = arg.getInt(Constants.LEVEL, 1);
        semester = arg.getInt(Constants.SEMESTER, 1);
        totalPoint = arg.getDouble(Constants.TOTAL_POINT, 0);
        totalUnit = arg.getDouble(Constants.TOTAL_UNIT, 0);
        Log.d(Constants.TAG, "calculate total point" + totalPoint + " total unit received" + totalUnit);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.gypee_calculate, container, false);
        gradingList = (NonScrollListView) v.findViewById(R.id.my_recycle);
        layout = (GridLayout) v.findViewById(R.id.my_grid);
        searchList = (ListView) v.findViewById(R.id.search_list);
        searchLayout = (LinearLayout) v.findViewById(R.id.search_container);
        addNewCourse = (Button) v.findViewById(R.id.search_add_button);
        scrollContainer = (ScrollView) v.findViewById(R.id.gypee_scrol);
        mCourseHelper = CourseHelper.getInstance();
        userGypee = new Gypee(totalUnit, totalPoint);

        int screenWidth = getResources().getConfiguration().screenWidthDp;
        int numberOfColumns = screenWidth / 100;
        layout.setColumnCount(numberOfColumns);

        searchList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Course course = searchAdapter.getItem(position);
                addCheckBox(course, layout);
                searchLayout.setVisibility(View.GONE);
                courseSearch.onActionViewCollapsed();
                menuItem.collapseActionView();
                hideKeyboard();
            }
        });
        searchList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                Course item = searchAdapter.getItem(i);
                CourseDetailsDialog dialog = CourseDetailsDialog.newInstance(item);
                dialog.show(getFragmentManager(), "");
                return true;
            }
        });
        addNewCourse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchLayout.setVisibility(View.GONE);
                courseSearch.onActionViewCollapsed();
                menuItem.collapseActionView();
                Intent intent = new Intent(getActivity(), AddCourseActivity.class);
                startActivity(intent);
            }
        });

        setHasOptionsMenu(true);
        process();
        FirebaseTracker.trackScreen(getActivity(), "GypeeCalculateFragment");
        return v;

    }

    public void process() {
        userCourse = mCourseHelper.getCourseList(level, semester);
        selectedCourse = new ArrayList<>();
        addCheckBox(userCourse, layout);

    }

    public void addCheckBox(Course course, GridLayout layout) {
        CheckBox checkBox = new CheckBox(getContext());
        checkBox.setText(course.getCourseName());
        checkBox.setOnCheckedChangeListener(new checkBoxListener(course.getCourseUnit(), course.getCourseName()));
        layout.addView(checkBox);

    }

    public void addCheckBox(List<Course> courses, GridLayout gridLayout) {
        for (Course course : courses) {
            addCheckBox(course, gridLayout);
        }
    }

    public int getTotalPoint(List<Course> array) {
        int myTotalPoint = 0;
        for (Course course : array) {
            myTotalPoint = myTotalPoint + (course.getGrade() * course.getCourseUnit());

        }
        return myTotalPoint;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_gypee_calculate, menu);
        menuItem = menu.findItem(R.id.menu_course_search);
        courseSearch = (SearchView) MenuItemCompat.getActionView(menuItem);
        courseSearch.setLayoutParams(new ActionBar.LayoutParams(Gravity.RIGHT));
        courseSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (!newText.equals("")) {
                    List<Course> tempSearchArray = removeDoubleEntry(mCourseHelper.searchByName(newText));
                    searchLayout.setVisibility(View.VISIBLE);
                    searchAdapter = new SuggestionAdapter(getActivity(), R.layout.course_search_item, tempSearchArray);
                    searchList.setAdapter(searchAdapter);
                }

                return true;

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_mark:
                if (tempTotalUnit != 0) {
                    double previousPoint = userGypee.getCtotalPoint();
                    double previousUnit = userGypee.getCtotalUnit();
                    int currentPoint = getTotalPoint(selectedCourse);
                    userGypee.setTotalUnit(tempTotalUnit);
                    userGypee.setTotalPoint(currentPoint);
                    userGypee.addToCgpa(currentPoint, tempTotalUnit);
                    totalUnit = userGypee.getCtotalUnit();
                    totalPoint = userGypee.getCtotalPoint();
                    String gpa = String.format(Locale.ENGLISH, "%.2f", userGypee.getGpa());
                    String cgpa = String.format(Locale.ENGLISH, "%.2f", userGypee.getCgpa());
                    mCallback.onDoneClick(semester, level, totalPoint, totalUnit, previousPoint, previousUnit, cgpa, gpa);
                } else {
                    Toast.makeText(getContext(), "You must select at least one Courses", Toast.LENGTH_SHORT).show();
                }
                break;
            case android.R.id.home:
                getActivity().finish();

        }
        return true;
    }

    public ArrayList<Course> removeDoubleEntry(List<Course> arrayList) {
        ArrayList<Course> truncatedArray = new ArrayList<>();
        for (Course entry : arrayList) {
            if (!isContained(truncatedArray, entry)) {
                truncatedArray.add(entry);
            }
        }
        return truncatedArray;
    }

    public boolean isContained(ArrayList<Course> array, Course course) {
        for (Course entry : array) {
            if (entry.getCourseName().equals(course.getCourseName())) {
                return true;
            }
        }
        return false;
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(courseSearch.getWindowToken(), 0);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    public interface Callback {
        public void onDoneClick(int semester, int level, double totalPoint, double totalUnit, double previousPoint, double previousUnit, String cgpa, String gpa);
    }

    public class checkBoxListener implements CompoundButton.OnCheckedChangeListener {

        Course tempCourse;

        public checkBoxListener(int unit, String name) {
            tempCourse = new Course(name, unit);

        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                selectedCourse.add(tempCourse);
                tempTotalUnit = tempTotalUnit + tempCourse.getCourseUnit();
            } else {
                selectedCourse.remove(tempCourse);
                tempTotalUnit = tempTotalUnit - tempCourse.getCourseUnit();
            }
            adapter = new GridAdapter(selectedCourse);
            gradingList.setAdapter(adapter);
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    scrollContainer.smoothScrollTo(0, scrollContainer.getBottom());
                }
            });
        }
    }

}
