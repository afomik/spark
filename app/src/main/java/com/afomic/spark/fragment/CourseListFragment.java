package com.afomic.spark.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afomic.spark.R;
import com.afomic.spark.util.FirebaseTracker;


/**
 * Created by afomic on 18-Oct-16.
 */
public class CourseListFragment extends Fragment {
    ViewPager pager;
    int level = 0;
    TabLayout tabs;
    String[] title = {"Electives", "Part One", "Part Two", "Part Three", "Part Four", "Part Five"};
    public static final String TAG = "courseFragment";

    public static CourseListFragment getInstance() {
        return new CourseListFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        //inflate the courselist layout
        View v = inflater.inflate(R.layout.course_list_home, container, false);

        AppCompatActivity act = (AppCompatActivity) getActivity();
        ActionBar actionBar = act.getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_hamburger);
            actionBar.setTitle("Course List");
        }
        FragmentManager fm = getFragmentManager();

        pager = (ViewPager) v.findViewById(R.id.single_pager);
        tabs = (TabLayout) v.findViewById(R.id.course_tab_layout);
        tabs.setupWithViewPager(pager);


        pager.setAdapter(new FragmentPagerAdapter(fm) {
            @Override
            public Fragment getItem(int position) {
                return CourseListDetailFragment.getInstance(position);
            }

            @Override
            public int getCount() {
                return 6;
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return title[position];
            }
        });

        pager.setCurrentItem(level);
        FirebaseTracker.trackScreen(getActivity(), "CourseListFragment");
        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
    }
}
