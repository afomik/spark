package com.afomic.spark.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.core.app.NotificationCompat;

import com.afomic.spark.R;
import com.afomic.spark.activities.MainActivity;
import com.afomic.spark.activities.TImeTableClassOverlayDialog;
import com.afomic.spark.data.Constants;
import com.afomic.spark.model.TimeTableClass;
import com.afomic.spark.util.Common;

import java.util.Locale;


public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle args = intent.getExtras();
        NotificationHelper alarm = new NotificationHelper(context);
        SharedPreferences sharedPref = androidx.preference.PreferenceManager.getDefaultSharedPreferences(context);
        boolean showAlarmDialog = sharedPref.getBoolean(context.getString(R.string.key_show_alarm_dialog), false);
        String timeElapseString = sharedPref.getString(context.getString(R.string.key_alarm_reminder_time), "10");
        int timeBeforeAlarm = Integer.parseInt(timeElapseString);
        String courseCode = args.getString(Constants.NAME);
        String courseVenue = args.getString(Constants.VENUE);
        int color = args.getInt(Constants.COLOR, Color.BLUE);
        int time = args.getInt(Constants.TIME);
        int date = args.getInt(Constants.DATE);
        String lecturer = args.getString(Constants.LECTURER);
        TimeTableClass item = new TimeTableClass(time, courseCode, courseVenue, color, date, lecturer);
        alarm.scheduleNext(item);
        if (showAlarmDialog) {
            Intent dialogIntent = new Intent(context, TImeTableClassOverlayDialog.class);
            dialogIntent.putExtra(Constants.EXTRA_TIME_TABLE_CLASS, item);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS
                    | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
            context.startActivity(dialogIntent);
            return;
        }

        NotificationManager mNotificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID = "TIMETABLE_CHANNEL";

        Common.createNotificationChannel(mNotificationManager,
                NOTIFICATION_CHANNEL_ID, "timeTableChannel",context.getString(R.string.notification_channel_description));
        Intent sentIntent = new Intent(context, MainActivity.class);
        sentIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pi = PendingIntent.getActivity(context, 232, sentIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID);
        builder.setContentTitle(String.format("%s at %s", courseCode, courseVenue));
        builder.setContentText(String.format(Locale.ENGLISH, "In %d minutes!!!", timeBeforeAlarm));
        builder.setAutoCancel(true);
        builder.setColor(color);
        builder.setPriority(Notification.PRIORITY_MAX);
        builder.setShowWhen(true);
        builder.setCategory(NotificationCompat.CATEGORY_SYSTEM);
        builder.setSmallIcon(R.drawable.notification_icon);
        //make the device vibrate
        builder.setDefaults(Notification.DEFAULT_VIBRATE);
        //make sound
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        builder.setSound(alarmSound);
        builder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
        if (Build.VERSION.SDK_INT >= 21) {
            builder.setVibrate(new long[]{1000, 1000, 1000, 1000, 1000});
        }
        builder.setContentIntent(pi);
        builder.setWhen(System.currentTimeMillis());
        mNotificationManager.notify(3432, builder.build());
    }
}
