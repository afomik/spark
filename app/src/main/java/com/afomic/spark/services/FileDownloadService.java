package com.afomic.spark.services;

import android.app.DownloadManager;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.widget.Toast;

import com.afomic.spark.data.Constants;
import com.afomic.spark.model.BlogPost;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.afomic.spark.Spark.getContext;

public class FileDownloadService extends IntentService {
    private static List<String> downloadRef = new ArrayList<>();
    private static final String name = "FileDownloadService";

    public FileDownloadService() {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        BlogPost blogPost = intent.getParcelableExtra(Constants.EXTRA_BLOG_POST);
        if (downloadRef.contains(blogPost.getId())) {
            Toast.makeText(FileDownloadService.this, "File is Already been downloaded", Toast.LENGTH_SHORT).show();
        } else {
            downloadFile(blogPost);
        }
    }

    public void downloadFile(BlogPost blogPost) {
        File direct = new File(Environment.getExternalStorageDirectory()
                + "/Nacoss/doc");
        if (!direct.exists()) {
            direct.mkdirs();
        }
        Uri file_uri = Uri.parse(blogPost.getFileUrl());
        DownloadManager downloadManager = (DownloadManager) getContext().getSystemService(Context.DOWNLOAD_SERVICE);
        DownloadManager.Request request = new DownloadManager.Request(file_uri);
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
        request.setAllowedOverRoaming(false);
        request.setTitle("Downloading " + blogPost.getTitle());
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setVisibleInDownloadsUi(true);
        request.setDestinationInExternalPublicDir("Nacoss/doc", blogPost.getTitle());
        downloadManager.enqueue(request);
        downloadRef.add(blogPost.getId());
    }
}
