package com.afomic.spark.adapter;

import android.content.Context;
import android.graphics.Typeface;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afomic.spark.R;
import com.afomic.spark.data.Constants;
import com.afomic.spark.data.PreferenceManager;
import com.afomic.spark.model.BlogPost;
import com.afomic.spark.model.NativeAd;
import com.afomic.spark.util.GlideApp;
import com.afomic.spark.util.PostHelper;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * Created by afomic on 9/21/17.
 */

public class PostAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private ArrayList<Object> mBlogPosts;
    private Typeface largeFont;
    private int[] fileTypeImageId = {R.drawable.doc, R.drawable.doc, R.drawable.pdf_logo, R.drawable.ppt};
    private static final int NATIVE_AD_VIEW_TYPE = 10;
    private BlogPostListener mListener;
    private PostHelper mPostHelper;

    public PostAdapter(Context context, ArrayList<Object> BlogPosts, BlogPostListener listener) {
        mBlogPosts = BlogPosts;
        mListener = listener;
        mContext = context;
        largeFont = Typeface.createFromAsset(mContext.getAssets(), "font/large.ttf");
        mPostHelper = new PostHelper(new PreferenceManager().getAssociationName());
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == BlogPost.Type.BLOG) {
            View v = LayoutInflater.from(mContext).inflate(R.layout.blog_post_item, parent, false);
            return new BlogViewHolder(v);
        } else if (viewType == BlogPost.Type.FILE) {
            View v = LayoutInflater.from(mContext).inflate(R.layout.file_post_item, parent, false);
            return new FileViewHolder(v);
        }
        View v = LayoutInflater.from(mContext).inflate(R.layout.native_ad_layout, parent, false);
        return new NativeViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        Object post = mBlogPosts.get(position);
        if (post instanceof BlogPost) {
            BlogPost tempBlogPost = (BlogPost) post;
            CharSequence time = DateUtils.getRelativeTimeSpanString(tempBlogPost.getTimeStamp());
            if (tempBlogPost.getType() == BlogPost.Type.BLOG) {
                final BlogViewHolder mHolder = (BlogViewHolder) holder;
                mHolder.blogTitle.setText(tempBlogPost.getTitle());
                mHolder.BlogPosterNameTextView.setText(tempBlogPost.getPosterName());
                mHolder.BlogPostTime.setText(time);
                FirebaseDatabase
                        .getInstance()
                        .getReference(Constants.POST_VIEW_REF)
                        .child(tempBlogPost.getId())
                        .addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (mHolder.totalViewCount != null) {
                                    mHolder.totalViewCount.setText(String.valueOf(dataSnapshot.getChildrenCount()));
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                FirebaseDatabase.getInstance()
                        .getReference(Constants.COMMENT_REF)
                        .orderByChild("blogId")
                        .equalTo(tempBlogPost.getId())
                        .addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (mHolder.totalCommentCount != null) {
                                    mHolder.totalCommentCount.setText(String.valueOf(dataSnapshot.getChildrenCount()));
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                FirebaseDatabase.getInstance()
                        .getReference(Constants.LIKES_REF)
                        .child(tempBlogPost.getId())
                        .addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (mHolder.totalRecommendationCount != null) {
                                    mHolder.totalRecommendationCount.setText(String.valueOf(dataSnapshot.getChildrenCount()));
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                GlideApp.with(mContext)
                        .load(tempBlogPost.getPictureUrl())
                        .placeholder(R.drawable.image_placeholder)
                        .into(mHolder.blogImage);
                GlideApp.with(mContext)
                        .load(tempBlogPost.getPosterIconUrl())
                        .placeholder(R.drawable.splash_icon)
                        .into(mHolder.BlogPosterIcon);

            } else {
                final FileViewHolder mHolder = (FileViewHolder) holder;
                mHolder.fileName.setText(tempBlogPost.getBody());
                mHolder.BlogPostTime.setText(String.valueOf(tempBlogPost.getTimeStamp()));
                mHolder.BlogPostTime.setText(time);
                if (tempBlogPost.isDownloading()) {
                    mHolder.progressLayout.setVisibility(View.VISIBLE);
                } else {
                    mHolder.progressLayout.setVisibility(View.GONE);
                }

                GlideApp.with(mContext)
                        .load(tempBlogPost.getPosterIconUrl())
                        .placeholder(R.drawable.default_logo)
                        .into(mHolder.BlogPosterIcon);
                GlideApp.with(mContext)
                        .load(fileTypeImageId[tempBlogPost.getFileType()])
                        .into(mHolder.fileIcon);
                mHolder.BlogPosterNameTextView.setText(tempBlogPost.getPosterName());
                FirebaseDatabase.getInstance()
                        .getReference(Constants.LIKES_REF)
                        .child(tempBlogPost.getId())
                        .addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (mHolder.downloadCount != null) {
                                    mHolder.downloadCount.setText(String.valueOf(dataSnapshot.getChildrenCount()));
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });

            }
        } else {
            mPostHelper.getRandomAd(new PostHelper.NativeAdCallBack() {
                @Override
                public void onSuccess(NativeAd nativeAd) {
                    mBlogPosts.remove(position);
                    mBlogPosts.add(position, nativeAd);
                    NativeViewHolder nativeViewHolder = (NativeViewHolder) holder;
                    GlideApp.with(mContext)
                            .load(nativeAd.getIconUrl())
                            .into(nativeViewHolder.adIcon);
                    GlideApp.with(mContext)
                            .load(nativeAd.getPictureUrl())
                            .into(nativeViewHolder.adImage);
                    nativeViewHolder.callToAction.setText(nativeAd.getCallToActionTitle());
                    nativeViewHolder.adDescription.setText(nativeAd.getDescription());
                    nativeViewHolder.adTitle.setText(nativeAd.getTitle());
                    FirebaseDatabase.getInstance()
                            .getReference(Constants.ADS_REF)
                            .child(nativeAd.getId())
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    NativeAd ad = dataSnapshot.getValue(NativeAd.class);
                                    FirebaseDatabase.getInstance()
                                            .getReference(Constants.ADS_REF)
                                            .child(ad.getId())
                                            .child("views")
                                            .setValue((ad.getViews() + 1));


                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
                }

                @Override
                public void onFailure(String message) {

                }
            });

        }


    }

    @Override
    public int getItemCount() {
        return mBlogPosts.size();
    }

    @Override
    public int getItemViewType(int position) {
        Object tempBlogPost = mBlogPosts.get(position);
        if (tempBlogPost instanceof BlogPost) {
            return ((BlogPost) tempBlogPost).getType();
        }
        return NATIVE_AD_VIEW_TYPE;
    }

    public interface BlogPostListener {
        void OnFileBlogPostClick(int position);

        void onBlogBlogPostClick(BlogPost blogPost);

        void onNativeAdActionClick(NativeAd nativeAd);

        void onSharePost(BlogPost post);
    }

    public class FileViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView BlogPosterNameTextView, BlogPostTime, fileName, downloadCount;
        ImageView fileIcon, BlogPosterIcon, downloadIcon, shareButton;
        LinearLayout progressLayout;

        public FileViewHolder(View itemView) {
            super(itemView);
            BlogPosterIcon = (ImageView) itemView.findViewById(R.id.imv_post_icon);
            fileIcon = (ImageView) itemView.findViewById(R.id.imv_file_icon);
            BlogPosterNameTextView = (TextView) itemView.findViewById(R.id.tv_poster_name);
            BlogPostTime = (TextView) itemView.findViewById(R.id.tv_post_time);
            fileName = (TextView) itemView.findViewById(R.id.tv_file_name);
            downloadIcon = itemView.findViewById(R.id.imv_download_icon);
            downloadCount = itemView.findViewById(R.id.tv_download_count);
            progressLayout = itemView.findViewById(R.id.progress);
            itemView.setOnClickListener(this);
            fileName.setTypeface(largeFont);
            BlogPosterNameTextView.setTypeface(largeFont);
            shareButton = itemView.findViewById(R.id.btn_share);
            shareButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    BlogPost blogPost = (BlogPost) mBlogPosts.get(getAdapterPosition());
                    mListener.onSharePost(blogPost);
                }
            });
        }

        @Override
        public void onClick(View view) {
            mListener.OnFileBlogPostClick(getAdapterPosition());
        }
    }

    public class BlogViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView BlogPosterNameTextView, BlogPostTime, blogTitle, totalCommentCount, totalViewCount, totalRecommendationCount;
        ImageView BlogPosterIcon, blogImage, shareButton;

        public BlogViewHolder(View itemView) {
            super(itemView);
            BlogPosterIcon = (ImageView) itemView.findViewById(R.id.imv_post_icon);
            BlogPosterNameTextView = (TextView) itemView.findViewById(R.id.tv_poster_name);
            BlogPostTime = (TextView) itemView.findViewById(R.id.tv_post_time);
            blogImage = (ImageView) itemView.findViewById(R.id.imv_blog_picture);
            blogTitle = (TextView) itemView.findViewById(R.id.tv_blog_title);
            totalCommentCount = (TextView) itemView.findViewById(R.id.tv_total_comment);
            totalRecommendationCount = itemView.findViewById(R.id.tv_total_recommendation);
            totalViewCount = itemView.findViewById(R.id.tv_total_view);
            shareButton = itemView.findViewById(R.id.btn_share);
            shareButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    BlogPost blogPost = (BlogPost) mBlogPosts.get(getAdapterPosition());
                    mListener.onSharePost(blogPost);
                }
            });
            blogTitle.setTypeface(largeFont);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            BlogPost blogBlogPost = (BlogPost) mBlogPosts.get(getAdapterPosition());
            mListener.onBlogBlogPostClick(blogBlogPost);

        }
    }

    public class NativeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView adTitle, adDescription;
        ImageView adImage, adIcon;
        Button callToAction;

        public NativeViewHolder(View itemView) {
            super(itemView);
            adImage = itemView.findViewById(R.id.imv_ad_picture);
            adTitle = itemView.findViewById(R.id.tv_ad_title);
            adDescription = itemView.findViewById(R.id.tv_ad_description);
            adIcon = itemView.findViewById(R.id.imv_ad_icon);
            callToAction = itemView.findViewById(R.id.btn_call_to_action);
            callToAction.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            NativeAd nativeAd = (NativeAd) mBlogPosts.get(getAdapterPosition());
            if (nativeAd.getId() != null) {
                FirebaseDatabase.getInstance()
                        .getReference(Constants.ADS_REF)
                        .child(nativeAd.getId())
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                NativeAd ad = dataSnapshot.getValue(NativeAd.class);
                                FirebaseDatabase.getInstance()
                                        .getReference(Constants.ADS_REF)
                                        .child(ad.getId())
                                        .child("conversion")
                                        .setValue((ad.getConversion() + 1));


                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
            }

            mListener.onNativeAdActionClick(nativeAd);

        }
    }

}
