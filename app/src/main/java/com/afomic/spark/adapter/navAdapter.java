package com.afomic.spark.adapter;

import android.content.Context;
import android.graphics.Typeface;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afomic.spark.R;
import com.afomic.spark.model.NavItem;

import java.util.ArrayList;


/**
 * Created by afomic on 30-Nov-16.
 */
public class navAdapter extends RecyclerView.Adapter<navAdapter.NavItemViewHolder> {
    private Context context;
    private ArrayList<NavItem> mNavItems;
    private int lastPositionSelected = 0;
    private NavItemListener mNavItemListener;

    public navAdapter(Context c, ArrayList<NavItem> navItems, NavItemListener navItemListener) {
        context = c;
        mNavItemListener = navItemListener;
        mNavItems = navItems;
    }

    @Override
    public NavItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.nav_item, parent, false);
        return new NavItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(NavItemViewHolder holder, int position) {
        NavItem navItem = mNavItems.get(position);
        holder.navText.setText(navItem.getTitle());
        if (navItem.isSelected()) {
            holder.navIcon.setImageResource(navItem.getSelectedDrawableIconId());
            holder.indication.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
            holder.navText.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        } else {
            holder.navIcon.setImageResource(navItem.getNormalDrawableIconId());
            holder.indication.setBackgroundColor(context.getResources().getColor(R.color.normalTextColor));
            holder.navText.setTextColor(context.getResources().getColor(R.color.normalTextColor));
        }

    }

    @Override
    public int getItemCount() {
        if (mNavItems != null) {
            return mNavItems.size();
        }
        return 0;
    }

    public interface NavItemListener {
        void onNavItemSelected(int position);

    }

    public class NavItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView navText;
        View indication;
        ImageView navIcon;

        public NavItemViewHolder(View itemView) {
            super(itemView);
            navText = itemView.findViewById(R.id.nav_title);
            Typeface typeface = Typeface.createFromAsset(context.getAssets(), "font/Lato-Regular.ttf");
            navText.setTypeface(typeface);
            indication = itemView.findViewById(R.id.nav_indicator);
            navIcon = itemView.findViewById(R.id.nav_icon);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            NavItem navItem = mNavItems.get(lastPositionSelected);
            navItem.setSelected(false);
            notifyItemChanged(lastPositionSelected);
            lastPositionSelected = getAdapterPosition();
            mNavItemListener.onNavItemSelected(getAdapterPosition());

        }
    }
}
