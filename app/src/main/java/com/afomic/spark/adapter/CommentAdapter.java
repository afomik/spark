package com.afomic.spark.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.text.format.DateUtils;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afomic.spark.R;
import com.afomic.spark.model.Comment;
import com.afomic.spark.util.SpringParser;

import java.util.List;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.CommentViewHolder> {

    private Context mContext;
    private List<Comment> mComments;
    private SpringParser mSpringParser = SpringParser.getInstance();

    public CommentAdapter(Context context, List<Comment> comments) {
        mComments = comments;
        mContext = context;
    }

    @Override
    public CommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View commentView = LayoutInflater.from(mContext).inflate(R.layout.item_comment, parent, false);
        return new CommentViewHolder(commentView);
    }

    @Override
    public void onBindViewHolder(CommentViewHolder holder, int position) {
        holder.bindView(position);
    }

    @Override
    public int getItemCount() {
        return mComments.size();
    }

    public class CommentViewHolder extends RecyclerView.ViewHolder {
        private TextView commentPosterNameTextView, commentTimeTextView;
        private TextView commentTextView;

        public CommentViewHolder(View itemView) {
            super(itemView);
            commentPosterNameTextView = itemView.findViewById(R.id.tv_poster_name);
            commentTimeTextView = itemView.findViewById(R.id.tv_comment_time);
            commentTextView = itemView.findViewById(R.id.tv_comment);
            commentTextView.setMovementMethod(LinkMovementMethod.getInstance());
            commentTextView.setLinkTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));

        }

        public void bindView(int position) {
            Comment comment = mComments.get(position);
            commentPosterNameTextView.setText(comment.getPosterName());
            commentTextView.setText(mSpringParser.parseString(comment.getContent()));
            commentTimeTextView.setText(DateUtils.getRelativeTimeSpanString(comment.getTimeStamp()));

        }
    }
}
