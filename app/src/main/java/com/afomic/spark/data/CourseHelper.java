package com.afomic.spark.data;

import androidx.annotation.NonNull;

import com.afomic.spark.model.Course;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class CourseHelper {
    private static CourseHelper sCourseHelper = null;
    private List<Course> allCourse = new ArrayList<>();
    private DatabaseReference courseDatabaseRef;
    private PreferenceManager preferenceManager = new PreferenceManager();

    private CourseHelper() {
        courseDatabaseRef = FirebaseDatabase.getInstance()
                .getReference()
                .child(Constants.COURSE_REF)
                .child(preferenceManager.getAssociationName());

        courseDatabaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                allCourse.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Course course = snapshot.getValue(Course.class);
                    allCourse.add(course);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public static CourseHelper getInstance() {
        if (sCourseHelper == null) {
            sCourseHelper = new CourseHelper();
        }
        return sCourseHelper;
    }

    public void forceUpdate() {
        sCourseHelper = new CourseHelper();
    }

    public List<Course> getCourseList(int level, int semester) {
        int option = preferenceManager.getOption();
        List<Course> result = new ArrayList<>();
        for (Course course : allCourse) {
            if (course.getCourseSemester() == semester && course.getCourseLevel() == level
                    && (course.getOption() == option || course.getOption() == 4)) {
                result.add(course);
            }
        }
        return result;
    }

    public List<Course> searchByName(String name) {
        List<Course> result = new ArrayList<>();
        for (Course course : allCourse) {
            if (course.getCourseName().toLowerCase().contains(name)) {
                result.add(course);
            }
        }
        return result;
    }

    public void addCourse(Course course) {
        String courseId = courseDatabaseRef.push().getKey();
        course.setId(courseId);
        courseDatabaseRef.child(courseId)
                .setValue(course);

    }
}
