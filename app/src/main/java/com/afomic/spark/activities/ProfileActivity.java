package com.afomic.spark.activities;

import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;

import com.afomic.spark.R;
import com.afomic.spark.data.Constants;
import com.afomic.spark.fragment.PersonViewerFragment;
import com.afomic.spark.util.FirebaseTracker;


public class ProfileActivity extends AppCompatActivity {
    String[] titles = {"Executive", "Parliament", "Lecturer"};
    String[] types = {Constants.EXCO, Constants.PARLIAMENT, Constants.LECTURER};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.profile_toolbar);
        setSupportActionBar(mToolbar);
        TabLayout tabs = (TabLayout) findViewById(R.id.profile_tab);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ViewPager pager = (ViewPager) findViewById(R.id.profile_pager);
        setTitle("Profiles");
        pager.setAdapter(new FragmentStatePagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return PersonViewerFragment.getInstance(position);
            }

            @Override
            public int getCount() {
                return 3;
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return titles[position];
            }
        });
        tabs.setupWithViewPager(pager);
        int position = getIntent().getIntExtra(Constants.TYPE, 0);
        pager.setCurrentItem(position);
        FirebaseTracker.trackScreen(ProfileActivity.this, "ProfileActivity");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
