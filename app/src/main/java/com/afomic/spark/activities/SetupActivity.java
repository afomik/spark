package com.afomic.spark.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.afomic.spark.R;
import com.afomic.spark.data.ConstitutionData;
import com.afomic.spark.data.PreferenceManager;
import com.afomic.spark.util.TextUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SetupActivity extends AppCompatActivity {
    @BindView(R.id.spn_departments)
    Spinner departmentSpinner;
    @BindView(R.id.edt_username)
    EditText usernameEditText;


    private int selectedDepartment = 0;
    private PreferenceManager mPreferenceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);
        ButterKnife.bind(SetupActivity.this);
        mPreferenceManager = new PreferenceManager();


        departmentSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedDepartment = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @OnClick(R.id.btn_submit_department)
    public void selectDepartment() {
        String username = usernameEditText.getText().toString();
        if (!username.isEmpty() && selectedDepartment != 0) {
            mPreferenceManager.setOption(selectedDepartment);
            mPreferenceManager.setUsername(username);
            mPreferenceManager.setUserId(TextUtil.getRandomString(16));
            startActivity(new Intent(SetupActivity.this, MainActivity.class));
            ConstitutionData.get(mPreferenceManager.getAssociationName());
            finish();
        } else {
            Toast.makeText(SetupActivity.this, "please fill appropriately", Toast.LENGTH_SHORT).show();
        }


    }
}

