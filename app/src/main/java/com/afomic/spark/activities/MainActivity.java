package com.afomic.spark.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;

import com.afomic.spark.R;
import com.afomic.spark.adapter.navAdapter;
import com.afomic.spark.fragment.ConstitutionFragment;
import com.afomic.spark.fragment.CourseListFragment;
import com.afomic.spark.fragment.FeedbackFragment;
import com.afomic.spark.fragment.GypeeFragment;
import com.afomic.spark.fragment.PostFragment;
import com.afomic.spark.fragment.ProfileFragment;
import com.afomic.spark.fragment.SettingsFragment;
import com.afomic.spark.fragment.TimeTableFragment;
import com.afomic.spark.model.NavItem;
import com.afomic.spark.services.NotificationHelper;
import com.afomic.spark.util.UploadDocument;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener, navAdapter.NavItemListener {
    private FragmentManager fm;
    private DrawerLayout drawerLayout;
    private RecyclerView navBar;
    private navAdapter adapter;
    private ArrayList<NavItem> navItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        drawerLayout = (DrawerLayout) findViewById(R.id.home);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_hamburger);
        fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.main_container);
        if (fragment == null) {
            PostFragment frag = PostFragment.newInstance();
            fm.beginTransaction().add(R.id.main_container, frag)
                    .addToBackStack(PostFragment.TAG).commit();
        }


        navBar = findViewById(R.id.nav_list);
        generateNaVList();
        navBar.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        navBar.addItemDecoration(new DividerItemDecoration(MainActivity.this, DividerItemDecoration.VERTICAL));
        adapter = new navAdapter(MainActivity.this, navItems, this);
        navBar.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(getString(R.string.key_alarm_reminder_time))) {
            NotificationHelper notificationHelper = new NotificationHelper(MainActivity.this);
            notificationHelper.resetAll();

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        androidx.preference.PreferenceManager.getDefaultSharedPreferences(this)
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        androidx.preference.PreferenceManager.getDefaultSharedPreferences(this)
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    public void displayFragment(Fragment frag, String tag) {
        drawerLayout.closeDrawers();
        fm.beginTransaction()
                .replace(R.id.main_container, frag)
                .addToBackStack(tag)
                .commit();

    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            drawerLayout.openDrawer(Gravity.LEFT);
        }
        return false;
    }

    @Override
    public void onNavItemSelected(int position) {
        switch (position) {
            case 0:
                PostFragment frag = PostFragment.newInstance();
                displayFragment(frag, PostFragment.TAG);
                break;
            case 1:
                TimeTableFragment timeTableFragment = TimeTableFragment.getInstance();
                displayFragment(timeTableFragment, TimeTableFragment.TAG);
                break;
            case 2:
                ProfileFragment profileFragment = new ProfileFragment();
                displayFragment(profileFragment, ProfileFragment.TAG);
                break;
            case 3:
                ConstitutionFragment constitutionFragment = ConstitutionFragment.getInstance();
                displayFragment(constitutionFragment, ConstitutionFragment.TAG);
                break;
            case 4:
                GypeeFragment gypeeFragment = GypeeFragment.getInstance();
                displayFragment(gypeeFragment, GypeeFragment.TAG);
                break;
            case 5:
                CourseListFragment courseListFragment = CourseListFragment.getInstance();
                displayFragment(courseListFragment, CourseListFragment.TAG);
                break;
            case 6:
                FeedbackFragment feedbackFragment = FeedbackFragment.newInstance();
                displayFragment(feedbackFragment, FeedbackFragment.TAG);
                break;
            case 7:
                SettingsFragment settingsFragment = SettingsFragment.newInstance();
                displayFragment(settingsFragment, SettingsFragment.TAG);
                break;

        }
        navItems.get(position).setSelected(true);
        adapter.notifyItemChanged(position);
    }

    private void generateNaVList() {
        navItems = new ArrayList<>();
        navItems.add(new NavItem("Home", R.drawable.home, R.drawable.home_click, true));
        navItems.add(new NavItem("Time Table", R.drawable.ic_date_normal, R.drawable.ic_date_selected, false));
        navItems.add(new NavItem("Profiles", R.drawable.ic_school, R.drawable.ic_school_selected, false));
        navItems.add(new NavItem("Constitution", R.drawable.ic_constitution, R.drawable.ic_constitution_selected, false));
        navItems.add(new NavItem("Gypee", R.drawable.ic_gypee, R.drawable.ic_gypee_selected, false));
        navItems.add(new NavItem("Course List", R.drawable.ic_course_list, R.drawable.ic_course_list_selected, false));
        navItems.add(new NavItem("Feedback", R.drawable.ic_feedback, R.drawable.ic_feedback_selected, false));
        navItems.add(new NavItem("Settings", R.drawable.ic_settings, R.drawable.ic_settings_selected, false));
    }

    @Override
    public void onBackPressed() {

        if (fm.getBackStackEntryCount() > 1) {
            FragmentManager.BackStackEntry backStackEntry = fm.getBackStackEntryAt(fm.getBackStackEntryCount() - 1);
            if (!backStackEntry.getName().equals(PostFragment.TAG)) {
                PostFragment frag = PostFragment.newInstance();
                displayFragment(frag, PostFragment.TAG);
            } else {
                openDrawerOrCloseApp();
            }
        } else {
            openDrawerOrCloseApp();

        }
    }

    public void openDrawerOrCloseApp() {
        if (drawerLayout.isDrawerOpen(Gravity.START)) {
            finish();
        } else {
            drawerLayout.openDrawer(Gravity.START, true);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
