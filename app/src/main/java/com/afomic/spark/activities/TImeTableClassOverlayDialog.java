package com.afomic.spark.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;

import com.afomic.spark.R;
import com.afomic.spark.data.Constants;
import com.afomic.spark.model.TimeTableClass;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TImeTableClassOverlayDialog extends Activity {
    @BindView(R.id.tv_detail_name)
    TextView courseCodeTextView;
    @BindView(R.id.tv_detail_time)
    TextView courseTimeTextView;
    @BindView(R.id.tv_detail_venue)
    TextView courseVenueTextView;
    @BindView(R.id.tv_detail_lecturer)
    TextView courseLecturerTextView;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_table_class_dialog);
        ButterKnife.bind(this);


        TimeTableClass timeTableClass = (TimeTableClass) getIntent().getSerializableExtra(Constants.EXTRA_TIME_TABLE_CLASS);
        courseCodeTextView.setText(timeTableClass.getName());
        courseCodeTextView.setBackgroundColor(timeTableClass.getColor());
        courseVenueTextView.setText(timeTableClass.getVenue());
        courseLecturerTextView.setText(timeTableClass.getLecturer());
        courseTimeTextView.setText(getTime(timeTableClass.getTime(), timeTableClass.getDate()));

    }

    @OnClick(R.id.action_snooze)
    public void onSnooze() {
        finish();
    }

    @OnClick(R.id.action_ok)
    public void onDismiss() {
        finish();
    }

    public String getTime(int time, int date) {
        String[] dates = {"MON", "TUE", "WED", "THUR", "FRI"};
        String ans;
        int startTime = time + 7;
        if (startTime < 12) {
            ans = String.format(Locale.ENGLISH, "%s, %d:00 - %d:00 am", dates[date], startTime, (startTime + 1));
        } else if (startTime == 12) {
            ans = dates[date] + ", 12:00 - 1:00 pm";
        } else {
            startTime = startTime % 12;
            ans = String.format(Locale.ENGLISH, "%s, %d:00 - %d:00 pm", dates[date], startTime, (startTime + 1));
        }
        return ans;
    }
}
