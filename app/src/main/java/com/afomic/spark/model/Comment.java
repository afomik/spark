package com.afomic.spark.model;

import android.os.Parcel;

public class Comment implements BlogElement {
    public static final Creator<Comment> CREATOR = new Creator<Comment>() {
        @Override
        public Comment createFromParcel(Parcel in) {
            return new Comment(in);
        }

        @Override
        public Comment[] newArray(int size) {
            return new Comment[size];
        }
    };
    public String id;
    private String content;
    private long timeStamp;
    private String posterName;
    private String blogId;

    public Comment() {

    }

    public Comment(String content, String posterName) {
        this.content = content;
        this.posterName = posterName;
    }

    protected Comment(Parcel in) {
        id = in.readString();
        content = in.readString();
        timeStamp = in.readLong();
    }

    @Override
    public int getType() {
        return Type.COMMENT;
    }

    @Override
    public String toHtml() {
        return null;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(content);
        dest.writeLong(timeStamp);
    }

    public String getBlogId() {
        return blogId;
    }

    public void setBlogId(String blogId) {
        this.blogId = blogId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getPosterName() {
        return posterName;
    }

    public void setPosterName(String posterName) {
        this.posterName = posterName;
    }
}
