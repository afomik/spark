package com.afomic.spark.model;

import android.os.Parcel;

public class AddComment implements BlogElement {
    public static final Creator<AddComment> CREATOR = new Creator<AddComment>() {
        @Override
        public AddComment createFromParcel(Parcel in) {
            return new AddComment(in);
        }

        @Override
        public AddComment[] newArray(int size) {
            return new AddComment[size];
        }
    };

    public AddComment() {

    }

    protected AddComment(Parcel in) {
    }

    @Override
    public int getType() {
        return Type.ADD_COMMENT;
    }

    @Override
    public String toHtml() {
        return null;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }
}
