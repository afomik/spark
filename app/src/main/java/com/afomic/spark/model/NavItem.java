package com.afomic.spark.model;

public class NavItem {
    private boolean selected = false;
    private String title;
    private int normalDrawableIconId;
    private int selectedDrawableIconId;

    public NavItem(String title, int normalDrawableIconId, int selectedDrawableIconId, boolean selected) {
        this.selected = selected;
        this.title = title;
        this.normalDrawableIconId = normalDrawableIconId;
        this.selectedDrawableIconId = selectedDrawableIconId;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getNormalDrawableIconId() {
        return normalDrawableIconId;
    }

    public void setNormalDrawableIconId(int normalDrawableIconId) {
        this.normalDrawableIconId = normalDrawableIconId;
    }

    public int getSelectedDrawableIconId() {
        return selectedDrawableIconId;
    }

    public void setSelectedDrawableIconId(int selectedDrawableIconId) {
        this.selectedDrawableIconId = selectedDrawableIconId;
    }
}
