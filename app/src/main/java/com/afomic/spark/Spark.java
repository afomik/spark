package com.afomic.spark;

import android.app.Application;
import android.content.Context;

import com.google.firebase.database.FirebaseDatabase;


/**
 * Created by afomic on 11/3/17.
 */

public class Spark extends Application {
    private static Context sContext;

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        sContext = getApplicationContext();
    }

    public static Context getContext() {
        return sContext;
    }
}
