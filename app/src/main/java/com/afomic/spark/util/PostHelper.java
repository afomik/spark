package com.afomic.spark.util;

import androidx.annotation.NonNull;

import com.afomic.spark.data.Constants;
import com.afomic.spark.model.BlogPost;
import com.afomic.spark.model.NativeAd;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class PostHelper {
    private List<BlogPost> allBlogPost;
    private List<NativeAd> allNativeAds;
    private String associationName;
    private boolean firstRun = true;

    public PostHelper(String associationName) {
        this.associationName = associationName;
        allBlogPost = new ArrayList<>();
        allNativeAds = new ArrayList<>();
    }

    public void getBlogPost(int page, final BlogPostCallBack callBack) {
        page = page - 1;
        final int pageStart = page * Constants.PAGE_COUNT;
        final int pageEnd = pageStart + Constants.PAGE_COUNT;
        if (allBlogPost.isEmpty()) {
            FirebaseDatabase.getInstance()
                    .getReference(Constants.POST_REF)
                    .child(associationName)
                    .orderByChild("status")
                    .equalTo(Constants.STATUS_APPROVED)
                    .addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            allBlogPost.clear();
                            List<Object> result = new ArrayList<>();
                            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                BlogPost post = snapshot.getValue(BlogPost.class);
                                allBlogPost.add(post);
                            }
                            savePosts(allBlogPost);
                            Collections.reverse(allBlogPost);
                            if (firstRun) {
                                if (pageEnd < allBlogPost.size()) {
                                    result.addAll(allBlogPost.subList(pageStart, pageEnd));
                                    result.add(new NativeAd());
                                    callBack.onSuccess(result);
                                } else {
                                    result.addAll(allBlogPost.subList(pageStart, allBlogPost.size()));
                                    result.add(new NativeAd());
                                    callBack.onSuccess(result);
                                }
                                firstRun = false;
                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            callBack.onFailure(databaseError.getMessage());
                        }
                    });

        } else if (pageStart < allBlogPost.size()) {
            List<Object> result = new ArrayList<>();
            if (pageEnd < allBlogPost.size()) {
                result.addAll(allBlogPost.subList(pageStart, pageEnd));
                result.add(new NativeAd());
                callBack.onSuccess(result);
            } else {
                result.addAll(allBlogPost.subList(pageStart, allBlogPost.size()));
                result.add(new NativeAd());
                callBack.onSuccess(result);
            }
        }


    }
    public void savePosts(List<BlogPost> profiles) {
        String constitutionString = Common.stringifyObject(profiles);
        Common.saveData(constitutionString, "post.txt");

    }

    public interface BlogPostCallBack {
        void onSuccess(List<Object> blogPosts);

        void onFailure(String message);
    }

    public interface NativeAdCallBack {
        void onSuccess(NativeAd nativeAd);

        void onFailure(String message);

    }

    public void getRandomAd(final NativeAdCallBack nativeAdCallBack) {
        final Random random = new Random();
        if (!allNativeAds.isEmpty()) {
            int position = random.nextInt(allNativeAds.size());
            nativeAdCallBack.onSuccess(allNativeAds.get(position));
        } else {
            FirebaseDatabase.getInstance()
                    .getReference(Constants.ADS_REF)
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if(dataSnapshot.hasChildren()){
                                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                    NativeAd nativeAd = snapshot.getValue(NativeAd.class);
                                    allNativeAds.add(nativeAd);
                                }
                                int position = random.nextInt(allNativeAds.size());
                                nativeAdCallBack.onSuccess(allNativeAds.get(position));
                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            nativeAdCallBack.onFailure(databaseError.getMessage());
                        }
                    });
        }
    }

}
