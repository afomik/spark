package com.afomic.spark.util;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.os.Environment;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.lang.reflect.Type;

public class Common {

    public static void createNotificationChannel(NotificationManager notificationManager, String channelID,
                                                 String channelName, String channelDescription) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(channelID,
                    channelName, NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.setDescription(channelDescription);
            notificationChannel.enableVibration(true);
            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            notificationManager.createNotificationChannel(notificationChannel);
        }
    }
    public static Object parseJSONToObject(String json, TypeToken objectType) {
        try {
            Gson gson = new GsonBuilder().create();
            return gson.fromJson(json, objectType.getType());
        } catch (JsonSyntaxException jse) {
            jse.printStackTrace();
            return null;
        }
    }
    public static String stringifyObject(Object object) {
        try {
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();
            return gson.toJson(object);
        } catch (OutOfMemoryError e) {
            return "";
        }
    }
    public static void saveData(String data, String name) {
        File f = new File(Environment.getExternalStorageDirectory(), "Nacoss");
        File file = new File(f, name);
        if (!file.exists()) {
            f.mkdir();
        }
        FileOutputStream os = null;
        try {

            os = new FileOutputStream(file);
            os.write(data.getBytes());
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public static String loadJSONFromAsset(Context context, String jsonFileName) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(jsonFileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

}
