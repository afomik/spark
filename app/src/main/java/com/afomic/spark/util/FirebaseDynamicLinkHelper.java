package com.afomic.spark.util;

import android.net.Uri;
import androidx.annotation.NonNull;

import com.afomic.spark.data.Constants;
import com.afomic.spark.model.BlogElement;
import com.afomic.spark.model.BlogPost;
import com.afomic.spark.model.NormalSizeTextElement;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;

import java.util.ArrayList;

/**
 * Created by afomic on 3/11/18.
 */

public class FirebaseDynamicLinkHelper {

    public static void getDynamicLink(BlogPost post, final DynamicLinkCallback callback) {

        Uri deepLink = new Uri.Builder()
                .scheme("https")
                .authority("sparkadmin.page.link")
                .appendPath("deeplinks")
                .appendQueryParameter(Constants.EXTRA_BLOG_ID, post.getId())
                .build();
        String domain = "https://sparkadmin.page.link";
        FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setDomainUriPrefix(domain)
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder("com.afomic.spark")
                        .setMinimumVersion(24)
                        .build())
                .setLink(deepLink)
                .setSocialMetaTagParameters(
                        new DynamicLink.SocialMetaTagParameters.Builder()
                                .setTitle(post.getTitle())
                                .setDescription(getDescription(post))
                                .setImageUrl(Uri.parse(post.getPictureUrl()))
                                .build())
                .buildShortDynamicLink()
                .addOnCompleteListener(new OnCompleteListener<ShortDynamicLink>() {
                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        if (task.isSuccessful()) {
                            // Short link created
                            Uri shortLink = task.getResult().getShortLink();
                            callback.onFinish(shortLink.toString());
                        } else {
                            // Error
                            // ...
                            callback.onError(task.getException().getMessage());
                        }
                    }
                });
    }

    private static String getDescription(BlogPost post) {
        ArrayList<BlogElement> blogElements = ElementParser.fromHtml(post.getBody());
        NormalSizeTextElement normalSizeTextElement = ElementParser.getFirstNormalText(blogElements);
        if (normalSizeTextElement != null) {
            return normalSizeTextElement.getBody();
        }
        return null;
    }

    public interface DynamicLinkCallback {
        void onFinish(String shortUrl);

        void onError(String error);
    }
}

