package com.afomic.spark.util;

import android.content.Intent;
import android.net.Uri;
import androidx.annotation.NonNull;
import android.text.style.ClickableSpan;
import android.view.View;

/**
 * Created by afomic on 29-Apr-17.
 */

public class LinkCLickSpan extends ClickableSpan {
    private String webPage;

    public LinkCLickSpan(String webPageUrl) {
        webPage = webPageUrl;
    }

    @Override
    public void onClick(@NonNull View widget) {
        Intent intent=new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(webPage));
        widget.getContext().startActivity(intent);
    }
}
