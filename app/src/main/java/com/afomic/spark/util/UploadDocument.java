package com.afomic.spark.util;

import android.content.Context;

import com.afomic.spark.data.Constants;
import com.afomic.spark.data.PreferenceManager;
import com.afomic.spark.model.BlogPost;
import com.afomic.spark.model.Constitution;
import com.afomic.spark.model.Course;
import com.afomic.spark.model.Profile;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

public class UploadDocument {
    private static String associationName;

    public static void uploadData(Context context) {
        associationName = new PreferenceManager().getAssociationName();
        uploadCourses(context);
        uploadDocument(context);
        uploadProfile(context);

    }

    public static void uploadConstitution(Context context) {
        String json = Common.loadJSONFromAsset(context, "constitution.json");
        List<Constitution> constitutions = (ArrayList<Constitution>) Common.parseJSONToObject(json,
                new TypeToken<ArrayList<Constitution>>() {
                });
        for (Constitution constitution : constitutions) {
            FirebaseDatabase.getInstance()
                    .getReference(Constants.CONSTITUTION_REF)
                    .child(associationName)
                    .push().setValue(constitution);
        }


    }

    public static void uploadCourses(Context context) {
        String json = Common.loadJSONFromAsset(context, "allCourse.json");
        List<Course> constitutions = (ArrayList<Course>) Common.parseJSONToObject(json,
                new TypeToken<ArrayList<Course>>() {
                });
        for (Course constitution : constitutions) {
            FirebaseDatabase.getInstance()
                    .getReference(Constants.COURSE_REF)
                    .child(associationName)
                    .push().setValue(constitution);
        }

    }

    public static void uploadProfile(Context context) {
        String json = Common.loadJSONFromAsset(context, "profile.json");
        List<Profile> constitutions = (ArrayList<Profile>) Common.parseJSONToObject(json,
                new TypeToken<ArrayList<Profile>>() {
                });
        for (Profile constitution : constitutions) {
            FirebaseDatabase.getInstance()
                    .getReference(Constants.EXTRA_PROFILE)
                    .child(associationName)
                    .push().setValue(constitution);
        }

    }

    public static void uploadDocument(Context context) {
        String json = Common.loadJSONFromAsset(context, "post.json");
        List<BlogPost> constitutions = (ArrayList<BlogPost>) Common.parseJSONToObject(json,
                new TypeToken<ArrayList<BlogPost>>() {
                });
        for (BlogPost constitution : constitutions) {
            FirebaseDatabase.getInstance()
                    .getReference(Constants.POST_REF)
                    .child(associationName)
                    .push().setValue(constitution);
        }

    }

}
